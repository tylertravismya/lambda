/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.*;

import com.occulue.primarykey.*;

import java.io.IOException;

import java.util.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * ResponseOption business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ResponseOption related services in the case of a ResponseOption business related service failing.</li>
 * <li>Exposes a simpler, uniform ResponseOption interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ResponseOption business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class ResponseOptionBusinessDelegate extends BaseBusinessDelegate {
    // AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
    // ~AIB

    //************************************************************************
    // Attributes
    //************************************************************************

    /**
     * Singleton instance
     */
    protected static ResponseOptionBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(ResponseOption.class.getName());

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public ResponseOptionBusinessDelegate() {
    }

    /**
         * ResponseOption Business Delegate Factory Method
         *
         * Returns a singleton instance of ResponseOptionBusinessDelegate().
         * All methods are expected to be self-sufficient.
         *
         * @return         ResponseOptionBusinessDelegate
         */
    public static ResponseOptionBusinessDelegate getResponseOptionInstance() {
        if (singleton == null) {
            singleton = new ResponseOptionBusinessDelegate();
        }

        return (singleton);
    }

    /**
     * Method to retrieve the ResponseOption via an ResponseOptionPrimaryKey.
     * @param         key
     * @return         ResponseOption
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException
     */
    public ResponseOption getResponseOption(ResponseOptionPrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ResponseOptionBusinessDelegate:getResponseOption - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        ResponseOption returnBO = null;

        ResponseOptionDAO dao = getResponseOptionDAO();

        try {
            returnBO = dao.findResponseOption(key);
        } catch (Exception exc) {
            String errMsg = "ResponseOptionBusinessDelegate:getResponseOption( ResponseOptionPrimaryKey key ) - unable to locate ResponseOption with key " +
                key.toString() + " - " + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseResponseOptionDAO(dao);
        }

        return returnBO;
    }

    /**
     * Method to retrieve a collection of all ResponseOptions
     *
     * @return         ArrayList<ResponseOption>
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<ResponseOption> getAllResponseOption()
        throws ProcessingException {
        String msgPrefix = "ResponseOptionBusinessDelegate:getAllResponseOption() - ";
        ArrayList<ResponseOption> array = null;

        ResponseOptionDAO dao = getResponseOptionDAO();

        try {
            array = dao.findAllResponseOption();
        } catch (Exception exc) {
            String errMsg = msgPrefix + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseResponseOptionDAO(dao);
        }

        return array;
    }

    /**
     * Creates the provided BO.
     * @param                businessObject         ResponseOption
     * @return       ResponseOption
     * @exception    ProcessingException
     * @exception        IllegalArgumentException
     */
    public ResponseOption createResponseOption(ResponseOption businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ResponseOptionBusinessDelegate:createResponseOption - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // return value once persisted
        ResponseOptionDAO dao = getResponseOptionDAO();

        try {
            businessObject = dao.createResponseOption(businessObject);
        } catch (Exception exc) {
            String errMsg = "ResponseOptionBusinessDelegate:createResponseOption() - Unable to create ResponseOption" +
                exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseResponseOptionDAO(dao);
        }

        return (businessObject);
    }

    /**
     * Saves the underlying BO.
     * @param                businessObject                ResponseOption
     * @return       what was just saved
     * @exception    ProcessingException
     * @exception          IllegalArgumentException
     */
    public ResponseOption saveResponseOption(ResponseOption businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ResponseOptionBusinessDelegate:saveResponseOption - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ResponseOptionPrimaryKey key = businessObject.getResponseOptionPrimaryKey();

        if (key != null) {
            ResponseOptionDAO dao = getResponseOptionDAO();

            try {
                businessObject = (ResponseOption) dao.saveResponseOption(businessObject);
            } catch (Exception exc) {
                String errMsg = "ResponseOptionBusinessDelegate:saveResponseOption() - Unable to save ResponseOption" +
                    exc;
                LOGGER.warning(errMsg);
                throw new ProcessingException(errMsg);
            } finally {
                releaseResponseOptionDAO(dao);
            }
        } else {
            String errMsg = "ResponseOptionBusinessDelegate:saveResponseOption() - Unable to create ResponseOption due to it having a null ResponseOptionPrimaryKey.";

            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        }

        return (businessObject);
    }

    /**
     * Deletes the associatied value object using the provided primary key.
     * @param                key         ResponseOptionPrimaryKey
     * @exception         ProcessingException
     */
    public void delete(ResponseOptionPrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ResponseOptionBusinessDelegate:saveResponseOption - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        ResponseOptionDAO dao = getResponseOptionDAO();

        try {
            dao.deleteResponseOption(key);
        } catch (Exception exc) {
            String errMsg = msgPrefix +
                "Unable to delete ResponseOption using key = " + key + ". " +
                exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseResponseOptionDAO(dao);
        }

        return;
    }

    // business methods
    /**
     * Returns the ResponseOption specific DAO.
     *
     * @return      ResponseOption DAO
     */
    public ResponseOptionDAO getResponseOptionDAO() {
        return (new com.occulue.dao.ResponseOptionDAO());
    }

    /**
     * Release the ResponseOptionDAO back to the FrameworkDAOFactory
     */
    public void releaseResponseOptionDAO(com.occulue.dao.ResponseOptionDAO dao) {
        dao = null;
    }
}
