/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import com.amazonaws.services.lambda.runtime.Context;

import com.occulue.bo.*;

import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

import com.occulue.primarykey.*;

import io.swagger.annotations.*;

import java.io.IOException;

import java.util.*;

import javax.ws.rs.*;

//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;


/**
 * ReferenceGroupLink AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ReferenceGroupLink related services in the case of a ReferenceGroupLink business related service failing.</li>
 * <li>Exposes a simpler, uniform ReferenceGroupLink interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ReferenceGroupLink business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "ReferenceGroupLink", description = "RESTful API to interact with ReferenceGroupLink resources.")
@Path("/ReferenceGroupLink")
public class ReferenceGroupLinkAWSLambdaDelegate extends BaseAWSLambdaDelegate {
    //************************************************************************
    // Attributes
    //************************************************************************

    //    private static final Logger LOGGER = Logger.getLogger(ReferenceGroupLinkAWSLambdaDelegate.class.getName());
    private static final String PACKAGE_NAME = "ReferenceGroupLink";

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public ReferenceGroupLinkAWSLambdaDelegate() {
    }

    /**
     * Creates the provided ReferenceGroupLink
     * @param                businessObject         ReferenceGroupLink
         * @param                context                Context
     * @return             ReferenceGroupLink
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a ReferenceGroupLink", notes = "Creates ReferenceGroupLink using the provided data")
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ReferenceGroupLink createReferenceGroupLink(
        @ApiParam(value = "ReferenceGroupLink entity to create", required = true)
    ReferenceGroupLink businessObject, Context context)
        throws CreationException {
        if (businessObject == null) {
            String errMsg = "Null ReferenceGroupLink provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        }

        try {
            String actionName = "save";
            String result = call(PACKAGE_NAME, actionName, businessObject);
            businessObject = (ReferenceGroupLink) fromJson(result,
                    ReferenceGroupLink.class);
        } catch (Exception exc) {
            String errMsg = "ReferenceGroupLinkAWSLambdaDelegate:createReferenceGroupLink() - Unable to create ReferenceGroupLink" +
                getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        } finally {
        }

        return (businessObject);
    }

    /**
     * Method to retrieve the ReferenceGroupLink via a supplied ReferenceGroupLinkPrimaryKey.
     * @param         key
         * @param        context                Context
     * @return         ReferenceGroupLink
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a ReferenceGroupLink", notes = "Gets the ReferenceGroupLink associated with the provided primary key", response = ReferenceGroupLink.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    public static ReferenceGroupLink getReferenceGroupLink(
        @ApiParam(value = "ReferenceGroupLink primary key", required = true)
    ReferenceGroupLinkPrimaryKey key, Context context)
        throws NotFoundException {
        ReferenceGroupLink businessObject = null;

        try {
            String actionName = "load";
            String result = call(PACKAGE_NAME, actionName, key);
            businessObject = (ReferenceGroupLink) fromJson(result,
                    ReferenceGroupLink.class);
        } catch (Exception exc) {
            String errMsg = "Unable to locate ReferenceGroupLink with key " +
                key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return businessObject;
    }

    /**
     * Saves the provided ReferenceGroupLink
     * @param                businessObject                ReferenceGroupLink
         * @param                context                Context
     * @return       what was just saved
     * @exception    SaveException
     */
    @ApiOperation(value = "Saves a ReferenceGroupLink", notes = "Saves ReferenceGroupLink using the provided data")
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ReferenceGroupLink saveReferenceGroupLink(
        @ApiParam(value = "ReferenceGroupLink entity to save", required = true)
    ReferenceGroupLink businessObject, Context context)
        throws SaveException {
        if (businessObject == null) {
            String errMsg = "Null ReferenceGroupLink provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferenceGroupLinkPrimaryKey key = businessObject.getReferenceGroupLinkPrimaryKey();

        if (key != null) {
            try {
                String actionName = "save";
                String result = call(PACKAGE_NAME, actionName, businessObject);
                businessObject = (ReferenceGroupLink) fromJson(result,
                        ReferenceGroupLink.class);
            } catch (Exception exc) {
                String errMsg = "Unable to save ReferenceGroupLink" +
                    getContextDetails(context) + exc;
                context.getLogger().log(errMsg);
                throw new SaveException(errMsg);
            } finally {
            }
        } else {
            String errMsg = "Unable to create ReferenceGroupLink due to it having a null ReferenceGroupLinkPrimaryKey.";
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        return (businessObject);
    }

    /**
    * Method to retrieve a collection of all ReferenceGroupLinks
    * @param                context                Context
    * @return         ArrayList<ReferenceGroupLink>
    */
    @ApiOperation(value = "Get all ReferenceGroupLink", notes = "Get all ReferenceGroupLink from storage", responseContainer = "ArrayList", response = ReferenceGroupLink.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public static ArrayList<ReferenceGroupLink> getAllReferenceGroupLink(
        Context context) throws NotFoundException {
        ArrayList<ReferenceGroupLink> array = null;

        try {
            String actionName = "viewAll";
            String result = call(PACKAGE_NAME, actionName, null);
            array = (ArrayList<ReferenceGroupLink>) fromJson(result,
                    ArrayList.class);
        } catch (Exception exc) {
            String errMsg = "failed to getAllReferenceGroupLink - " +
                getContextDetails(context) + exc.getMessage();
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return array;
    }

    /**
     * Deletes the associated business object using the provided primary key.
     * @param                key         ReferenceGroupLinkPrimaryKey
     * @param                context                Context
     * @exception         DeletionException
     */
    @ApiOperation(value = "Deletes a ReferenceGroupLink", notes = "Deletes the ReferenceGroupLink associated with the provided primary key", response = ReferenceGroupLink.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public static void deleteReferenceGroupLink(
        @ApiParam(value = "ReferenceGroupLink primary key", required = true)
    ReferenceGroupLinkPrimaryKey key, Context context)
        throws DeletionException {
        if (key == null) {
            String errMsg = "Null key provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        }

        try {
            String actionName = "delete";
            String result = call(PACKAGE_NAME, actionName, key);
        } catch (Exception exc) {
            String errMsg = "Unable to delete ReferenceGroupLink using key = " +
                key + ". " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        } finally {
        }

        return;
    }

    // role related methods

    /**
     * Gets the ReferrerGroup using the provided primary key of a ReferenceGroupLink
     * @param                parentKey        ReferenceGroupLinkPrimaryKey
     * @return            ReferrerGroup
     * @exception        NotFoundException
     */
    public static ReferrerGroup getReferrerGroup(
        ReferenceGroupLinkPrimaryKey parentKey, Context context)
        throws NotFoundException {
        ReferenceGroupLink referenceGroupLink = getReferenceGroupLink(parentKey,
                context);
        ReferrerGroupPrimaryKey childKey = referenceGroupLink.getReferrerGroup()
                                                             .getReferrerGroupPrimaryKey();
        ReferrerGroup child = ReferrerGroupAWSLambdaDelegate.getReferrerGroup(childKey,
                context);

        return (child);
    }

    /**
     * Assigns the ReferrerGroup on a ReferenceGroupLink using the provided primary key of a ReferrerGroup
     * @param                parentKey        ReferenceGroupLinkPrimaryKey
     * @param                parentKey        ReferenceGroupLinkPrimaryKey
     * @param                context                Context
     * @return            ReferenceGroupLink
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public static ReferenceGroupLink saveReferrerGroup(
        ReferenceGroupLinkPrimaryKey parentKey,
        ReferrerGroupPrimaryKey childKey, Context context)
        throws SaveException, NotFoundException {
        ReferenceGroupLink referenceGroupLink = getReferenceGroupLink(parentKey,
                context);
        ReferrerGroup child = ReferrerGroupAWSLambdaDelegate.getReferrerGroup(childKey,
                context);

        // assign the ReferrerGroup
        referenceGroupLink.setReferrerGroup(child);

        // save the ReferenceGroupLink 
        referenceGroupLink = ReferenceGroupLinkAWSLambdaDelegate.saveReferenceGroupLink(referenceGroupLink,
                context);

        return (referenceGroupLink);
    }

    /**
     * Unassigns the ReferrerGroup on a ReferenceGroupLink
     * @param                parentKey        ReferenceGroupLinkPrimaryKey
     * @param                Context                context
     * @return            ReferenceGroupLink
     * @exception        SaveException
     * @exception        NotFoundException
         * @exception        SaveException
     */
    public static ReferenceGroupLink deleteReferrerGroup(
        ReferenceGroupLinkPrimaryKey parentKey, Context context)
        throws DeletionException, NotFoundException, SaveException {
        ReferenceGroupLink referenceGroupLink = getReferenceGroupLink(parentKey,
                context);

        if (referenceGroupLink.getReferrerGroup() != null) {
            ReferrerGroupPrimaryKey pk = referenceGroupLink.getReferrerGroup()
                                                           .getReferrerGroupPrimaryKey();

            // first null out the ReferrerGroup on the parent so there's no constraint during deletion
            referenceGroupLink.setReferrerGroup(null);
            ReferenceGroupLinkAWSLambdaDelegate.saveReferenceGroupLink(referenceGroupLink,
                context);

            // now it is safe to delete the ReferrerGroup 
            ReferrerGroupAWSLambdaDelegate.deleteReferrerGroup(pk, context);
        }

        return (referenceGroupLink);
    }

    /**
     * Gets the LinkProvider using the provided primary key of a ReferenceGroupLink
     * @param                parentKey        ReferenceGroupLinkPrimaryKey
     * @return            User
     * @exception        NotFoundException
     */
    public static User getLinkProvider(ReferenceGroupLinkPrimaryKey parentKey,
        Context context) throws NotFoundException {
        ReferenceGroupLink referenceGroupLink = getReferenceGroupLink(parentKey,
                context);
        UserPrimaryKey childKey = referenceGroupLink.getLinkProvider()
                                                    .getUserPrimaryKey();
        User child = UserAWSLambdaDelegate.getUser(childKey, context);

        return (child);
    }

    /**
     * Assigns the LinkProvider on a ReferenceGroupLink using the provided primary key of a User
     * @param                parentKey        ReferenceGroupLinkPrimaryKey
     * @param                parentKey        ReferenceGroupLinkPrimaryKey
     * @param                context                Context
     * @return            ReferenceGroupLink
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public static ReferenceGroupLink saveLinkProvider(
        ReferenceGroupLinkPrimaryKey parentKey, UserPrimaryKey childKey,
        Context context) throws SaveException, NotFoundException {
        ReferenceGroupLink referenceGroupLink = getReferenceGroupLink(parentKey,
                context);
        User child = UserAWSLambdaDelegate.getUser(childKey, context);

        // assign the LinkProvider
        referenceGroupLink.setLinkProvider(child);

        // save the ReferenceGroupLink 
        referenceGroupLink = ReferenceGroupLinkAWSLambdaDelegate.saveReferenceGroupLink(referenceGroupLink,
                context);

        return (referenceGroupLink);
    }

    /**
     * Unassigns the LinkProvider on a ReferenceGroupLink
     * @param                parentKey        ReferenceGroupLinkPrimaryKey
     * @param                Context                context
     * @return            ReferenceGroupLink
     * @exception        SaveException
     * @exception        NotFoundException
         * @exception        SaveException
     */
    public static ReferenceGroupLink deleteLinkProvider(
        ReferenceGroupLinkPrimaryKey parentKey, Context context)
        throws DeletionException, NotFoundException, SaveException {
        ReferenceGroupLink referenceGroupLink = getReferenceGroupLink(parentKey,
                context);

        if (referenceGroupLink.getLinkProvider() != null) {
            UserPrimaryKey pk = referenceGroupLink.getLinkProvider()
                                                  .getUserPrimaryKey();

            // first null out the User on the parent so there's no constraint during deletion
            referenceGroupLink.setLinkProvider(null);
            ReferenceGroupLinkAWSLambdaDelegate.saveReferenceGroupLink(referenceGroupLink,
                context);

            // now it is safe to delete the LinkProvider 
            UserAWSLambdaDelegate.deleteUser(pk, context);
        }

        return (referenceGroupLink);
    }
}
