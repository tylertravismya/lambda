/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import com.amazonaws.services.lambda.runtime.Context;

import com.occulue.bo.*;

import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

import com.occulue.primarykey.*;

import io.swagger.annotations.*;

import java.io.IOException;

import java.util.*;

import javax.ws.rs.*;

//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;


/**
 * ReferenceGiver AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ReferenceGiver related services in the case of a ReferenceGiver business related service failing.</li>
 * <li>Exposes a simpler, uniform ReferenceGiver interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ReferenceGiver business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "ReferenceGiver", description = "RESTful API to interact with ReferenceGiver resources.")
@Path("/ReferenceGiver")
public class ReferenceGiverAWSLambdaDelegate extends BaseAWSLambdaDelegate {
    //************************************************************************
    // Attributes
    //************************************************************************

    //    private static final Logger LOGGER = Logger.getLogger(ReferenceGiverAWSLambdaDelegate.class.getName());
    private static final String PACKAGE_NAME = "ReferenceGiver";

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public ReferenceGiverAWSLambdaDelegate() {
    }

    /**
     * Creates the provided ReferenceGiver
     * @param                businessObject         ReferenceGiver
         * @param                context                Context
     * @return             ReferenceGiver
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a ReferenceGiver", notes = "Creates ReferenceGiver using the provided data")
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ReferenceGiver createReferenceGiver(
        @ApiParam(value = "ReferenceGiver entity to create", required = true)
    ReferenceGiver businessObject, Context context) throws CreationException {
        if (businessObject == null) {
            String errMsg = "Null ReferenceGiver provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        }

        try {
            String actionName = "save";
            String result = call(PACKAGE_NAME, actionName, businessObject);
            businessObject = (ReferenceGiver) fromJson(result,
                    ReferenceGiver.class);
        } catch (Exception exc) {
            String errMsg = "ReferenceGiverAWSLambdaDelegate:createReferenceGiver() - Unable to create ReferenceGiver" +
                getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        } finally {
        }

        return (businessObject);
    }

    /**
     * Method to retrieve the ReferenceGiver via a supplied ReferenceGiverPrimaryKey.
     * @param         key
         * @param        context                Context
     * @return         ReferenceGiver
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a ReferenceGiver", notes = "Gets the ReferenceGiver associated with the provided primary key", response = ReferenceGiver.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    public static ReferenceGiver getReferenceGiver(
        @ApiParam(value = "ReferenceGiver primary key", required = true)
    ReferenceGiverPrimaryKey key, Context context) throws NotFoundException {
        ReferenceGiver businessObject = null;

        try {
            String actionName = "load";
            String result = call(PACKAGE_NAME, actionName, key);
            businessObject = (ReferenceGiver) fromJson(result,
                    ReferenceGiver.class);
        } catch (Exception exc) {
            String errMsg = "Unable to locate ReferenceGiver with key " +
                key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return businessObject;
    }

    /**
     * Saves the provided ReferenceGiver
     * @param                businessObject                ReferenceGiver
         * @param                context                Context
     * @return       what was just saved
     * @exception    SaveException
     */
    @ApiOperation(value = "Saves a ReferenceGiver", notes = "Saves ReferenceGiver using the provided data")
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ReferenceGiver saveReferenceGiver(
        @ApiParam(value = "ReferenceGiver entity to save", required = true)
    ReferenceGiver businessObject, Context context) throws SaveException {
        if (businessObject == null) {
            String errMsg = "Null ReferenceGiver provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferenceGiverPrimaryKey key = businessObject.getReferenceGiverPrimaryKey();

        if (key != null) {
            try {
                String actionName = "save";
                String result = call(PACKAGE_NAME, actionName, businessObject);
                businessObject = (ReferenceGiver) fromJson(result,
                        ReferenceGiver.class);
            } catch (Exception exc) {
                String errMsg = "Unable to save ReferenceGiver" +
                    getContextDetails(context) + exc;
                context.getLogger().log(errMsg);
                throw new SaveException(errMsg);
            } finally {
            }
        } else {
            String errMsg = "Unable to create ReferenceGiver due to it having a null ReferenceGiverPrimaryKey.";
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        return (businessObject);
    }

    /**
    * Method to retrieve a collection of all ReferenceGivers
    * @param                context                Context
    * @return         ArrayList<ReferenceGiver>
    */
    @ApiOperation(value = "Get all ReferenceGiver", notes = "Get all ReferenceGiver from storage", responseContainer = "ArrayList", response = ReferenceGiver.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public static ArrayList<ReferenceGiver> getAllReferenceGiver(
        Context context) throws NotFoundException {
        ArrayList<ReferenceGiver> array = null;

        try {
            String actionName = "viewAll";
            String result = call(PACKAGE_NAME, actionName, null);
            array = (ArrayList<ReferenceGiver>) fromJson(result, ArrayList.class);
        } catch (Exception exc) {
            String errMsg = "failed to getAllReferenceGiver - " +
                getContextDetails(context) + exc.getMessage();
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return array;
    }

    /**
     * Deletes the associated business object using the provided primary key.
     * @param                key         ReferenceGiverPrimaryKey
     * @param                context                Context
     * @exception         DeletionException
     */
    @ApiOperation(value = "Deletes a ReferenceGiver", notes = "Deletes the ReferenceGiver associated with the provided primary key", response = ReferenceGiver.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public static void deleteReferenceGiver(
        @ApiParam(value = "ReferenceGiver primary key", required = true)
    ReferenceGiverPrimaryKey key, Context context) throws DeletionException {
        if (key == null) {
            String errMsg = "Null key provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        }

        try {
            String actionName = "delete";
            String result = call(PACKAGE_NAME, actionName, key);
        } catch (Exception exc) {
            String errMsg = "Unable to delete ReferenceGiver using key = " +
                key + ". " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        } finally {
        }

        return;
    }

    // role related methods

    /**
     * Gets the QuestionGroup using the provided primary key of a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @return            QuestionGroup
     * @exception        NotFoundException
     */
    public static QuestionGroup getQuestionGroup(
        ReferenceGiverPrimaryKey parentKey, Context context)
        throws NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);
        QuestionGroupPrimaryKey childKey = referenceGiver.getQuestionGroup()
                                                         .getQuestionGroupPrimaryKey();
        QuestionGroup child = QuestionGroupAWSLambdaDelegate.getQuestionGroup(childKey,
                context);

        return (child);
    }

    /**
     * Assigns the QuestionGroup on a ReferenceGiver using the provided primary key of a QuestionGroup
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                context                Context
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public static ReferenceGiver saveQuestionGroup(
        ReferenceGiverPrimaryKey parentKey, QuestionGroupPrimaryKey childKey,
        Context context) throws SaveException, NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);
        QuestionGroup child = QuestionGroupAWSLambdaDelegate.getQuestionGroup(childKey,
                context);

        // assign the QuestionGroup
        referenceGiver.setQuestionGroup(child);

        // save the ReferenceGiver 
        referenceGiver = ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

        return (referenceGiver);
    }

    /**
     * Unassigns the QuestionGroup on a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                Context                context
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
         * @exception        SaveException
     */
    public static ReferenceGiver deleteQuestionGroup(
        ReferenceGiverPrimaryKey parentKey, Context context)
        throws DeletionException, NotFoundException, SaveException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);

        if (referenceGiver.getQuestionGroup() != null) {
            QuestionGroupPrimaryKey pk = referenceGiver.getQuestionGroup()
                                                       .getQuestionGroupPrimaryKey();

            // first null out the QuestionGroup on the parent so there's no constraint during deletion
            referenceGiver.setQuestionGroup(null);
            ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

            // now it is safe to delete the QuestionGroup 
            QuestionGroupAWSLambdaDelegate.deleteQuestionGroup(pk, context);
        }

        return (referenceGiver);
    }

    /**
     * Gets the User using the provided primary key of a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @return            User
     * @exception        NotFoundException
     */
    public static User getUser(ReferenceGiverPrimaryKey parentKey,
        Context context) throws NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);
        UserPrimaryKey childKey = referenceGiver.getUser().getUserPrimaryKey();
        User child = UserAWSLambdaDelegate.getUser(childKey, context);

        return (child);
    }

    /**
     * Assigns the User on a ReferenceGiver using the provided primary key of a User
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                context                Context
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public static ReferenceGiver saveUser(ReferenceGiverPrimaryKey parentKey,
        UserPrimaryKey childKey, Context context)
        throws SaveException, NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);
        User child = UserAWSLambdaDelegate.getUser(childKey, context);

        // assign the User
        referenceGiver.setUser(child);

        // save the ReferenceGiver 
        referenceGiver = ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

        return (referenceGiver);
    }

    /**
     * Unassigns the User on a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                Context                context
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
         * @exception        SaveException
     */
    public static ReferenceGiver deleteUser(
        ReferenceGiverPrimaryKey parentKey, Context context)
        throws DeletionException, NotFoundException, SaveException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);

        if (referenceGiver.getUser() != null) {
            UserPrimaryKey pk = referenceGiver.getUser().getUserPrimaryKey();

            // first null out the User on the parent so there's no constraint during deletion
            referenceGiver.setUser(null);
            ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

            // now it is safe to delete the User 
            UserAWSLambdaDelegate.deleteUser(pk, context);
        }

        return (referenceGiver);
    }

    /**
     * Gets the Referrer using the provided primary key of a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @return            Referrer
     * @exception        NotFoundException
     */
    public static Referrer getReferrer(ReferenceGiverPrimaryKey parentKey,
        Context context) throws NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);
        ReferrerPrimaryKey childKey = referenceGiver.getReferrer()
                                                    .getReferrerPrimaryKey();
        Referrer child = ReferrerAWSLambdaDelegate.getReferrer(childKey, context);

        return (child);
    }

    /**
     * Assigns the Referrer on a ReferenceGiver using the provided primary key of a Referrer
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                context                Context
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public static ReferenceGiver saveReferrer(
        ReferenceGiverPrimaryKey parentKey, ReferrerPrimaryKey childKey,
        Context context) throws SaveException, NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);
        Referrer child = ReferrerAWSLambdaDelegate.getReferrer(childKey, context);

        // assign the Referrer
        referenceGiver.setReferrer(child);

        // save the ReferenceGiver 
        referenceGiver = ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

        return (referenceGiver);
    }

    /**
     * Unassigns the Referrer on a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                Context                context
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
         * @exception        SaveException
     */
    public static ReferenceGiver deleteReferrer(
        ReferenceGiverPrimaryKey parentKey, Context context)
        throws DeletionException, NotFoundException, SaveException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);

        if (referenceGiver.getReferrer() != null) {
            ReferrerPrimaryKey pk = referenceGiver.getReferrer()
                                                  .getReferrerPrimaryKey();

            // first null out the Referrer on the parent so there's no constraint during deletion
            referenceGiver.setReferrer(null);
            ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

            // now it is safe to delete the Referrer 
            ReferrerAWSLambdaDelegate.deleteReferrer(pk, context);
        }

        return (referenceGiver);
    }

    /**
     * Gets the LastQuestionAnswered using the provided primary key of a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @return            Question
     * @exception        NotFoundException
     */
    public static Question getLastQuestionAnswered(
        ReferenceGiverPrimaryKey parentKey, Context context)
        throws NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);
        QuestionPrimaryKey childKey = referenceGiver.getLastQuestionAnswered()
                                                    .getQuestionPrimaryKey();
        Question child = QuestionAWSLambdaDelegate.getQuestion(childKey, context);

        return (child);
    }

    /**
     * Assigns the LastQuestionAnswered on a ReferenceGiver using the provided primary key of a Question
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                context                Context
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public static ReferenceGiver saveLastQuestionAnswered(
        ReferenceGiverPrimaryKey parentKey, QuestionPrimaryKey childKey,
        Context context) throws SaveException, NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);
        Question child = QuestionAWSLambdaDelegate.getQuestion(childKey, context);

        // assign the LastQuestionAnswered
        referenceGiver.setLastQuestionAnswered(child);

        // save the ReferenceGiver 
        referenceGiver = ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

        return (referenceGiver);
    }

    /**
     * Unassigns the LastQuestionAnswered on a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                Context                context
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
         * @exception        SaveException
     */
    public static ReferenceGiver deleteLastQuestionAnswered(
        ReferenceGiverPrimaryKey parentKey, Context context)
        throws DeletionException, NotFoundException, SaveException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);

        if (referenceGiver.getLastQuestionAnswered() != null) {
            QuestionPrimaryKey pk = referenceGiver.getLastQuestionAnswered()
                                                  .getQuestionPrimaryKey();

            // first null out the Question on the parent so there's no constraint during deletion
            referenceGiver.setLastQuestionAnswered(null);
            ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

            // now it is safe to delete the LastQuestionAnswered 
            QuestionAWSLambdaDelegate.deleteQuestion(pk, context);
        }

        return (referenceGiver);
    }

    /**
     * Retrieves the Answers on a ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                context                Context
     * @return            Set<Answer>
     * @exception        NotFoundException
     */
    public static Set<Answer> getAnswers(ReferenceGiverPrimaryKey parentKey,
        Context context) throws NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);

        return (referenceGiver.getAnswers());
    }

    /**
     * Add the assigned Answer into the Answers of the relevant ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                childKey        AnswerPrimaryKey
         * @param                context                Context
     * @return            ReferenceGiver
     * @exception        NotFoundException
     */
    public static ReferenceGiver addAnswers(
        ReferenceGiverPrimaryKey parentKey, AnswerPrimaryKey childKey,
        Context context) throws SaveException, NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);

        // find the Answer
        Answer child = AnswerAWSLambdaDelegate.getAnswer(childKey, context);

        // add it to the Answers 
        referenceGiver.getAnswers().add(child);

        // save the ReferenceGiver
        referenceGiver = ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

        return (referenceGiver);
    }

    /**
     * Saves multiple Answer entities as the Answers to the relevant ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                List<AnswerPrimaryKey> childKeys
     * @return            ReferenceGiver
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public ReferenceGiver assignAnswers(ReferenceGiverPrimaryKey parentKey,
        List<AnswerPrimaryKey> childKeys, Context context)
        throws SaveException, NotFoundException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);

        // clear out the Answers 
        referenceGiver.getAnswers().clear();

        // finally, find each child and add
        if (childKeys != null) {
            Answer child = null;

            for (AnswerPrimaryKey childKey : childKeys) {
                // retrieve the Answer
                child = AnswerAWSLambdaDelegate.getAnswer(childKey, context);

                // add it to the Answers List
                referenceGiver.getAnswers().add(child);
            }
        }

        // save the ReferenceGiver
        referenceGiver = ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                context);

        return (referenceGiver);
    }

    /**
     * Delete multiple Answer entities as the Answers to the relevant ReferenceGiver
     * @param                parentKey        ReferenceGiverPrimaryKey
     * @param                List<AnswerPrimaryKey> childKeys
     * @return            ReferenceGiver
     * @exception        DeletionException
     * @exception        NotFoundException
     * @exception        SaveException
     */
    public ReferenceGiver deleteAnswers(ReferenceGiverPrimaryKey parentKey,
        List<AnswerPrimaryKey> childKeys, Context context)
        throws DeletionException, NotFoundException, SaveException {
        ReferenceGiver referenceGiver = getReferenceGiver(parentKey, context);

        if (childKeys != null) {
            Set<Answer> children = referenceGiver.getAnswers();
            Answer child = null;

            for (AnswerPrimaryKey childKey : childKeys) {
                try {
                    // first remove the relevant child from the list
                    child = AnswerAWSLambdaDelegate.getAnswer(childKey, context);
                    children.remove(child);

                    // then safe to delete the child				
                    AnswerAWSLambdaDelegate.deleteAnswer(childKey, context);
                } catch (Exception exc) {
                    String errMsg = "Deletion failed - " + exc.getMessage();
                    context.getLogger().log(errMsg);
                    throw new DeletionException(errMsg);
                }
            }

            // assign the modified list of Answer back to the referenceGiver
            referenceGiver.setAnswers(children);
            // save it 
            referenceGiver = ReferenceGiverAWSLambdaDelegate.saveReferenceGiver(referenceGiver,
                    context);
        }

        return (referenceGiver);
    }
}
