/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import com.occulue.bo.*;

import com.occulue.delegate.*;

import com.occulue.primarykey.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;

import java.io.*;

import java.util.*;
import java.util.logging.*;


/**
 * Test User class.
 *
 * @author    dev@realmethods.com
 */
public class UserTest {
    // attributes 
    protected UserPrimaryKey thePrimaryKey = null;
    protected Properties frameworkProperties = null;
    private final Logger LOGGER = Logger.getLogger(User.class.getName());
    private Handler handler = null;
    private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";

    // constructors
    public UserTest() {
        LOGGER.setUseParentHandlers(false); // only want to output to the provided LogHandler
    }

    // test methods
    @Test
    /**
     * Full Create-Read-Update-Delete of a User, through a UserTest.
     */
    public void testCRUD() throws Throwable {
        try {
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("Beginning full test on UserTest...");

            testCreate();
            testRead();
            testUpdate();
            testGetAll();
            testDelete();

            LOGGER.info("Successfully ran a full test on UserTest...");
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("");
        } catch (Throwable e) {
            throw e;
        } finally {
            if (handler != null) {
                handler.flush();
                LOGGER.removeHandler(handler);
            }
        }
    }

    /**
     * Tests creating a new User.
     *
     * @return    User
     */
    public User testCreate() throws Throwable {
        User businessObject = null;

        {
            LOGGER.info("UserTest:testCreate()");
            LOGGER.info("-- Attempting to create a User");

            StringBuilder msg = new StringBuilder("-- Failed to create a User");

            try {
                businessObject = UserBusinessDelegate.getUserInstance()
                                                     .createUser(getNewBO());
                assertNotNull(businessObject, msg.toString());

                thePrimaryKey = (UserPrimaryKey) businessObject.getUserPrimaryKey();
                assertNotNull(thePrimaryKey,
                    msg.toString() + " Contains a null primary key");

                LOGGER.info("-- Successfully created a User with primary key" +
                    thePrimaryKey);
            } catch (Exception e) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning(msg.toString() + businessObject);

                throw e;
            }
        }

        return businessObject;
    }

    /**
     * Tests reading a User.
     *
     * @return    User
     */
    public User testRead() throws Throwable {
        LOGGER.info("UserTest:testRead()");
        LOGGER.info("-- Reading a previously created User");

        User businessObject = null;
        StringBuilder msg = new StringBuilder(
                "-- Failed to read User with primary key");
        msg.append(thePrimaryKey);

        try {
            businessObject = UserBusinessDelegate.getUserInstance()
                                                 .getUser(thePrimaryKey);

            assertNotNull(businessObject, msg.toString());

            LOGGER.info("-- Successfully found User " +
                businessObject.toString());
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests updating a User.
     *
     * @return    User
     */
    public User testUpdate() throws Throwable {
        LOGGER.info("UserTest:testUpdate()");
        LOGGER.info("-- Attempting to update a User.");

        StringBuilder msg = new StringBuilder("Failed to update a User : ");
        User businessObject = null;

        try {
            businessObject = testCreate();

            assertNotNull(businessObject, msg.toString());

            LOGGER.info("-- Now updating the created User.");

            // for use later on...
            thePrimaryKey = (UserPrimaryKey) businessObject.getUserPrimaryKey();

            UserBusinessDelegate proxy = UserBusinessDelegate.getUserInstance();
            businessObject = proxy.saveUser(businessObject);

            assertNotNull(businessObject, msg.toString());

            LOGGER.info("-- Successfully saved User - " +
                businessObject.toString());
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : primarykey-" + thePrimaryKey +
                " : businessObject-" + businessObject + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests deleting a User.
     */
    public void testDelete() throws Throwable {
        LOGGER.info("UserTest:testDelete()");
        LOGGER.info("-- Deleting a previously created User.");

        try {
            UserBusinessDelegate.getUserInstance().delete(thePrimaryKey);

            LOGGER.info("-- Successfully deleted User with primary key " +
                thePrimaryKey);
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning("-- Failed to delete User with primary key " +
                thePrimaryKey);

            throw e;
        }
    }

    /**
     * Tests getting all Users.
     *
     * @return    Collection
     */
    public ArrayList<User> testGetAll() throws Throwable {
        LOGGER.info("UserTest:testGetAll() - Retrieving Collection of Users:");

        StringBuilder msg = new StringBuilder("-- Failed to get all User : ");
        ArrayList<User> collection = null;

        try {
            // call the static get method on the UserBusinessDelegate
            collection = UserBusinessDelegate.getUserInstance().getAllUser();

            if ((collection == null) || (collection.size() == 0)) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning("-- " + msg.toString() +
                    " Empty collection returned.");
            } else {
                // Now print out the values
                User currentBO = null;
                Iterator<User> iter = collection.iterator();

                while (iter.hasNext()) {
                    // Retrieve the businessObject   
                    currentBO = iter.next();

                    assertNotNull(currentBO,
                        "-- null value object in Collection.");
                    assertNotNull(currentBO.getUserPrimaryKey(),
                        "-- value object in Collection has a null primary key");

                    LOGGER.info(" - " + currentBO.toString());
                }
            }
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString());

            throw e;
        }

        return (collection);
    }

    public UserTest setHandler(Handler handler) {
        this.handler = handler;
        LOGGER.addHandler(handler); // assign so the LOGGER can only output results to the Handler

        return this;
    }

    /**
     * Returns a new populate User
     *
     * @return    User
     */
    protected User getNewBO() {
        User newBO = new User();

        // AIB : \#defaultBOOutput() 
        newBO.setPassword(new String(
                org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10)));
        newBO.setResumeLinkUrl(new String(
                org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10)));
        newBO.setLinkedInUrl(new String(
                org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10)));

        // ~AIB
        return (newBO);
    }
}
